# SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

variables:
  APP_ID: io.gitlab.liferooter.TextPieces.Devel
  APP_NAME: textpieces
  BUNDLE: ${APP_NAME}-nightly.flatpak
  MANIFEST_PATH: build-aux/${APP_ID}.json
  RUNTIME_REPO: https://dl.flathub.org/repo/flathub.flatpakrepo
  BUILDDIR: builddir

rustfmt:
  image: "rust:slim"
  script:
    - rustup component add rustfmt
    # Create blank versions of our configured files
    # so rustfmt does not yell about non-existent files or completely empty files
    - echo -e "" >> src/config.rs
    - rustc -Vv && cargo -Vv
    - cargo fmt --version
    - cargo fmt --all -- --color=always --check

cargo-clippy:
  image: quay.io/gnome_infrastructure/gnome-runtime-images:gnome-46
  variables:
    FLATPAK_MODULE: ${APP_NAME}
  script:
    - flatpak-builder --keep-build-dirs --user --disable-rofiles-fuse ${BUILDDIR} --stop-at=${FLATPAK_MODULE} ${MANIFEST_PATH}
    - flatpak build-finish --share=network ${BUILDDIR}
    - echo "cargo clippy -- -D warnings" | flatpak-builder --user --disable-rofiles-fuse --build-shell=${FLATPAK_MODULE} ${BUILDDIR} ${MANIFEST_PATH}

flatpak:
  image: quay.io/gnome_infrastructure/gnome-runtime-images:gnome-46
  script:
    - flatpak-builder --keep-build-dirs --user --disable-rofiles-fuse ${BUILDDIR} --repo=repo ${BRANCH:+--default-branch=$BRANCH} ${MANIFEST_PATH}
    - flatpak build-bundle repo ${BUNDLE} --runtime-repo=${RUNTIME_REPO} ${APP_ID} ${BRANCH}
  artifacts:
    name: Flatpak artifacts
    expose_as: Get Flatpak bundle here
    when: always
    paths:
      - ${BUNDLE}
    expire_in: 14 days

nix:
  image: docker.io/nixos/nix:latest
  script:
    - echo "experimental-features = nix-command flakes" >> /etc/nix/nix.conf
    - nix build .
