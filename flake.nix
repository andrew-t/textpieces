# SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

{
  inputs.fenv-src = {
    url = "gitlab:ZanderBrown/fenv?host=gitlab.gnome.org";
    flake = false;
  };
  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      fenv-src,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        devShells.flatpak = import ./flatpak-shell.nix { inherit pkgs fenv-src; };
        packages = rec {
          default = textpieces;
          textpieces = pkgs.callPackage ./package.nix {};
        };
        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
