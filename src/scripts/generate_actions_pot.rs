// SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: CC0-1.0

//! This binary generates lines for gettext .pot file
//! with strings from built-in actions.

use std::collections::BTreeSet;

use pollster::block_on;
use textpieces_core::Actions;

fn main() {
    let provider = textpieces_core::collection::provider();
    let actions = block_on(Actions::new(provider)).unwrap();
    let actions = actions.actions();

    actions
        .iter()
        .flat_map(|(_, action)| {
            action
                .parameters
                .iter()
                .map(|p| &p.label)
                .chain([&action.name, &action.description])
        })
        .collect::<BTreeSet<_>>()
        .into_iter()
        .for_each(|s| {
            println!(
                "\n\
                 #. Translators: generated from built-in actions\n\
                 msgctxt \"action\"\n\
                 msgid \"{s}\"\n\
                 msgstr \"\""
            )
        });
}
