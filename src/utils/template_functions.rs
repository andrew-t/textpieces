// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::glib;

/// Set of common template callbacks.
pub struct TemplateFunctions;

#[gtk::template_callbacks]
impl TemplateFunctions {
    /// Conditional expression.
    ///
    /// If `cond` is `true`, returns `when`, else returns `unless`.
    #[template_callback]
    fn cond(_: glib::Object, cond: bool, when: String, unless: String) -> String {
        if cond {
            when
        } else {
            unless
        }
    }

    /// Logical `not`.
    #[template_callback]
    fn not(_: glib::Object, arg: bool) -> bool {
        !arg
    }

    /// Logical `and`.
    #[template_callback]
    fn and(_: glib::Object, a: bool, b: bool) -> bool {
        a && b
    }

    /// Logical `or`.
    #[template_callback]
    fn or(_: glib::Object, a: bool, b: bool) -> bool {
        a || b
    }

    /// Checks if the string is empty.
    #[template_callback]
    fn is_empty(_: glib::Object, s: &str) -> bool {
        s.is_empty()
    }
}
