// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gettextrs::gettext;
use gtk::gio;

use gio::prelude::*;

/// Returns [`gio::ListModel`] of [`gtk::FileFilter`]s for file chooser dialogs.
///
/// The model contains two file filters.
/// One is "Text Files" and another is "All files".
pub fn file_filters() -> gio::ListModel {
    let text_files = gtk::FileFilter::new();
    text_files.add_mime_type("text/plain");
    text_files.set_name(Some(&gettext("Text Files")));

    let all_files = gtk::FileFilter::new();
    all_files.add_mime_type("*");
    all_files.set_name(Some(&gettext("All Files")));

    let model = gio::ListStore::new::<gtk::FileFilter>();
    model.append(&text_files);
    model.append(&all_files);

    model.upcast()
}
