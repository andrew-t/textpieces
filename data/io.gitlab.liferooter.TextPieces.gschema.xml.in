<?xml version="1.0" encoding="utf-8"?>

<!--
SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

<schemalist gettext-domain="@gettext-package@">
  <schema path="/io/gitlab/liferooter/TextPieces/" id="@app-id@">
    <!-- Appearance -->
    <key name="color-scheme" type="s">
      <choices>
        <choice value="follow" />
        <choice value="light" />
        <choice value="dark" />
      </choices>
      <default>'follow'</default>
      <summary>Color scheme</summary>
    </key>

    <key name="font-family" type="s">
      <default>'Monospace'</default>
      <summary>Font family</summary>
    </key>

    <key name="font-size" type="u">
      <default>12</default>
      <summary>Font size</summary>
    </key>

    <!-- Saved window state -->
    <key name="width" type="i">
      <default>800</default>
      <summary>Window width</summary>
    </key>

    <key name="height" type="i">
      <default>500</default>
      <summary>Window height</summary>
    </key>

    <key name="is-maximized" type="b">
      <default>false</default>
      <summary>Window maximized state</summary>
    </key>

    <!-- Last used action -->
    <key name="last-action-id" type="s">
      <default>""</default>
      <summary>The last used action ID</summary>
    </key>

    <key name="last-params" type="a(sv)">
      <default>[]</default>
      <summary>Parameter values of the last used action</summary>
    </key>

    <!-- Editor behavior -->
    <key name="wrap-lines" type="b">
      <default>true</default>
      <summary>Whether to wrap long lines</summary>
    </key>

    <key name="tabs-to-spaces" type="b">
      <default>true</default>
      <summary>Whether to insert spaces instead of tabs</summary>
    </key>

    <key name="spaces-in-tab" type="u">
      <default>4</default>
      <summary>How many spaces are in a tab character</summary>
    </key>

    <!-- Search bar state -->
    <key name="search-at-word-boundaries" type="b">
      <default>false</default>
      <summary>Whether a search match must start and end a word.</summary>
    </key>

    <key name="search-case-sensitive" type="b">
      <default>false</default>
      <summary>Whether the search is case sensitive.</summary>
    </key>

    <key name="search-regex-enabled" type="b">
      <default>false</default>
      <summary>Whether to search by regular expressions.</summary>
    </key>
  </schema>
</schemalist>
